package main

import (
	"encoding/json"
	"fmt"
	"log"
	"reflect"
	"strings"
)

const (
	CHANGE DiffType = "change"
	ADD    DiffType = "add"
	REMOVE DiffType = "remove"
)

type (
	DiffType  string
	DiffField interface{}
	DiffPath  []interface{}
)

type DiffResult struct {
	DiffType      DiffType
	First, Second DiffField
	Path          DiffPath
	PathDot       string
	PathDotStar   string
	// Key           string
	// KeyInt        int
}
type DiffResults []DiffResult

func MakeDiffResult(diffType DiffType, first DiffField, second DiffField, path DiffPath) DiffResult {
	return DiffResult{
		DiffType:    diffType,
		First:       first,
		Second:      second,
		Path:        path,
		PathDot:     makePathDot(path, false),
		PathDotStar: makePathDot(path, true),
		// Key:         fmt.Sprintf("%s-%s", diffType, makePathDot(path, false)),
		// KeyInt:      -1,
	}
}

// func SortDiffResult(diffResultArray DiffResult) DiffResult {
// 	sort.SliceStable(diffResultArray, func(i, j string) bool {
// 		return diffResultArray[i].Key < diffResultArray[j].Key
// 	})
// 	return diffResultArray
// }

func JsonLoads(data string) interface{} {
	var parsed interface{}
	err := json.Unmarshal([]byte(data), &parsed)
	if err != nil {
		panic(err)
	}
	return parsed
}

func JsonDumps(data interface{}) string {
	dumped_byte, err := json.Marshal(&data)
	if err != nil {
		panic(err)
	}
	return string(dumped_byte)
}

func GetMapAddition(termFirst map[string]interface{}, termSecond map[string]interface{}) map[string]interface{} {
	result := make(map[string]interface{})
	for key, value := range termSecond {
		if _, exist := termFirst[key]; !exist {
			result[key] = value
		}
	}
	return result
}

func GetArrayAddition(termFirst []interface{}, termSecond []interface{}) map[int]interface{} {
	result := make(map[int]interface{})
	if len(termSecond) > len(termFirst) {
		for index := len(termFirst); index < len(termSecond); index++ {
			result[index] = termSecond[index]
		}
	}
	return result
}

func makePathDot(path DiffPath, stared bool) string {
	var dotPathArray []string
	for _, key := range path {
		switch keyType := key.(type) {
		case int:
			if stared {
				dotPathArray = append(dotPathArray, "[*]")
			} else {
				dotPathArray = append(dotPathArray, "["+fmt.Sprint(key.(int))+"]")
			}
		case string:
			dotPathArray = append(dotPathArray, "."+fmt.Sprint(key.(string)))
		default:
			log.Fatal("could not get type element:", keyType)
		}
	}
	return strings.Trim(strings.Join(dotPathArray, ""), ".")
}

// func isArrayComparable() {  // todo
// }

func IsTypeEqual(first DiffField, second DiffField) bool {
	if first == nil && second == nil {
		return true
	} else if first != nil && second == nil {
		return false
	} else if first == nil && second != nil {
		return false
	}
	firstType := reflect.TypeOf(first).Kind()
	secondType := reflect.TypeOf(second).Kind()
	if firstType != secondType {
		// log.Println("got different types: first -", firstType, "; second -", secondType)  // todo
		return false
	}
	return true
}

func compareNumber(first DiffField, second DiffField, path DiffPath) (results DiffResults) {
	// log.Println("type: number, path:", path, "first:", first, "second:", second)  // todo
	if IsTypeEqual(first, second) && first == second {
		return
	} else {
		return DiffResults{MakeDiffResult(CHANGE, first, second, path)}
	}
}

func compareString(first DiffField, second DiffField, path DiffPath) (results DiffResults) {
	// log.Println("type: string, path:", path, "first:", first, "second:", second)  // todo
	if IsTypeEqual(first, second) && first == second {
		return
	} else {
		return DiffResults{MakeDiffResult(CHANGE, first, second, path)}
	}
}

func compareBool(first DiffField, second DiffField, path DiffPath) (results DiffResults) {
	// log.Println("type: boolean, path:", path, "first:", first, "second:", second)  // todo
	if IsTypeEqual(first, second) && first == second {
		return
	} else {
		return DiffResults{MakeDiffResult(CHANGE, first, second, path)}
	}
}

func compareNull(first DiffField, second DiffField, path DiffPath) (results DiffResults) {
	// log.Println("type: null, path:", path, "first: null", "second:", second)  // todo
	if IsTypeEqual(first, second) {
		return
	} else {
		return DiffResults{MakeDiffResult(CHANGE, first, second, path)}
	}
}

func compareArray(first DiffField, second DiffField, path DiffPath) (results DiffResults) {
	// log.Println("type: array, path:", path, "first:", first, "second:", second)  // todo
	if IsTypeEqual(first, second) {
		firstArray := first.([]interface{})
		secondArray := second.([]interface{})

		for index, firstValue := range firstArray {
			value_path := append(path, index)
			exist := index < len(secondArray)
			if exist {
				results_tmp := CompareUnknown(firstValue, secondArray[index], value_path)
				results = append(results, results_tmp...)
			} else {
				result := MakeDiffResult(REMOVE, firstValue, nil, value_path)
				results = append(results, result)
			}
		}

		added := GetArrayAddition(firstArray, secondArray)
		for key, secondValueAdded := range added {
			valuePath := append(path, key)
			result := MakeDiffResult(ADD, nil, secondValueAdded, valuePath)
			results = append(results, result)
		}

	} else {
		result := MakeDiffResult(CHANGE, first, second, path)
		results = append(results, result)
	}
	return results
}
func compareObject(first DiffField, second DiffField, path DiffPath) (results DiffResults) {
	// log.Println("type: object, path:", path, "first:", first, "second:", second)  // todo
	if IsTypeEqual(first, second) {
		firstObject := first.(map[string]interface{})
		secondObject := second.(map[string]interface{})

		for key, firstValue := range firstObject {
			valuePath := append(path, key)
			secondValue, exist := secondObject[key]
			if exist {
				resultsTmp := CompareUnknown(firstValue, secondValue, valuePath)
				results = append(results, resultsTmp...)
			} else {
				result := MakeDiffResult(REMOVE, firstValue, nil, valuePath)
				results = append(results, result)
			}
		}

		added := GetMapAddition(firstObject, secondObject)
		for key, secondValueAdded := range added {
			valuePath := append(path, key)
			result := MakeDiffResult(ADD, nil, secondValueAdded, valuePath)
			results = append(results, result)
		}

	} else {
		result := MakeDiffResult(CHANGE, first, second, path)
		results = append(results, result)
	}
	return results
}

func CompareUnknown(first DiffField, second DiffField, path DiffPath) DiffResults { //  todo ADD type
	var results DiffResults = DiffResults{}
	switch firstType := first.(type) {
	case float64:
		results = append(results, compareNumber(first, second, path)...)
	case string:
		results = append(results, compareString(first, second, path)...)
	case bool:
		results = append(results, compareBool(first, second, path)...)
	case nil:
		results = append(results, compareNull(first, second, path)...)
	case []interface{}:
		results = append(results, compareArray(first, second, path)...)
	case map[string]interface{}:
		results = append(results, compareObject(first, second, path)...)
	default:
		log.Fatal("could not get type json element:", firstType)
	}
	return results
}

// type Comparator struct {  // todo
// 	first, second diffObject
// 	// only           []interface{}
// 	// convert_first  []interface{}
// 	// convert_second []interface{}
// }

func Compare(first DiffField, second DiffField) DiffResults {
	path := make([]interface{}, 0)
	return CompareUnknown(first, second, path)
}

func CompareJSON(jsonFirst string, jsonSecond string) string {
	first, second := JsonLoads(jsonFirst), JsonLoads(jsonSecond)
	results := Compare(first, second)
	// log.Println("results:", results, "len:", len(results))  // todo
	return JsonDumps(results)
}
