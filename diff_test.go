package main

import (
	"reflect"
	"testing"
)

// func TestExample(t *testing.T) { // todo remove minimal test example
// 	tests := []struct {
// 		first  string
// 		second string
// 		result string
// 	}{
// 		{`{"a":1}`, `{"a":2}`, `[{"DiffType":"change","First":1,"Second":2,"Path":["a"],"PathDot":"a","PathDotStar":"a"}]`},
// 	}
// 	for _, tt := range tests {
// 		expected := JsonLoads(tt.result)
// 		result := JsonLoads(CompareJSON(tt.first, tt.second))
// 		if !reflect.DeepEqual(expected, result) {
// 			t.Error("wrong DiffResults \nexpected:", expected, "\ngot:     ", result)
// 		}
// 	}
// }

// func TestGenerate(t *testing.T) { // todo remove
// 	tests := []struct {
// 		first  string
// 		second string
// 	}{
// 		{`{"a":1}`, `{"a":2}`},
// 	}
// 	for _, tt := range tests {
// 		result := CompareJSON(tt.first, tt.second)
// 		fmt.Println("{`" + tt.first + "`, `" + tt.second + "`, `" + result + "`},")
// 	}
// }

func TestNoDiff(t *testing.T) {
	tests := []struct {
		first           string
		second          string
		expected_result string
	}{
		{`"a"`, `"a"`, `[]`},
		{`1`, `1`, `[]`},
		{`1`, `1.0`, `[]`},
		{`1.1`, `1.1`, `[]`},
		{`false`, `false`, `[]`},
		{`true`, `true`, `[]`},
		{`null`, `null`, `[]`},
		{`{"a":1}`, `{"a":1}`, `[]`},
		{`{"a":""}`, `{"a":""}`, `[]`},
		{`{"a":"a"}`, `{"a":"a"}`, `[]`},
		{`[1,2,3]`, `[1,2,3]`, `[]`},
		{`{}`, `{}`, `[]`},
		{`[]`, `[]`, `[]`},
	}
	for _, tt := range tests {
		expected := JsonLoads(tt.expected_result)
		result := JsonLoads(CompareJSON(tt.first, tt.second))
		if !reflect.DeepEqual(expected, result) {
			t.Error("wrong TestNoDiff result, case:", tt, "\nexpected:", expected, "\ngot:     ", result)
		}
	}
}

func TestChange(t *testing.T) {
	tests := []struct {
		first           string
		second          string
		expected_result string
	}{
		{`{"a":1}`, `{"a":1.001}`, `[{"DiffType":"change","First":1,"Second":1.001,"Path":["a"],"PathDot":"a","PathDotStar":"a"}]`},
		{`{"a":1}`, `{"a":"1"}`, `[{"DiffType":"change","First":1,"Second":"1","Path":["a"],"PathDot":"a","PathDotStar":"a"}]`},
		{`{"a":1}`, `{"a":false}`, `[{"DiffType":"change","First":1,"Second":false,"Path":["a"],"PathDot":"a","PathDotStar":"a"}]`},
		{`{"a":1}`, `{"a":true}`, `[{"DiffType":"change","First":1,"Second":true,"Path":["a"],"PathDot":"a","PathDotStar":"a"}]`},
		{`{"a":1}`, `{"a":null}`, `[{"DiffType":"change","First":1,"Second":null,"Path":["a"],"PathDot":"a","PathDotStar":"a"}]`},
		{`{"a":""}`, `{"a":"v"}`, `[{"DiffType":"change","First":"","Second":"v","Path":["a"],"PathDot":"a","PathDotStar":"a"}]`},
		{`{"a":"v"}`, `{"a":""}`, `[{"DiffType":"change","First":"v","Second":"","Path":["a"],"PathDot":"a","PathDotStar":"a"}]`},
		{`{"a":null}`, `{"a":1}`, `[{"DiffType":"change","First":null,"Second":1,"Path":["a"],"PathDot":"a","PathDotStar":"a"}]`},
		{`{"a":1}`, `{"a":{}}`, `[{"DiffType":"change","First":1,"Second":{},"Path":["a"],"PathDot":"a","PathDotStar":"a"}]`},
		{`{"a":1}`, `{"a":{"b":{"c":1}}}`, `[{"DiffType":"change","First":1,"Second":{"b":{"c":1}},"Path":["a"],"PathDot":"a","PathDotStar":"a"}]`},
		{`{"a":1}`, `{"a":{"b":{"c":{"d":1}}}}`, `[{"DiffType":"change","First":1,"Second":{"b":{"c":{"d":1}}},"Path":["a"],"PathDot":"a","PathDotStar":"a"}]`},
		{`{"a":{"b":1}}`, `{"a":{"b":2}}`, `[{"DiffType":"change","First":1,"Second":2,"Path":["a","b"],"PathDot":"a.b","PathDotStar":"a.b"}]`},
		{`{"a":{"b":1}}`, `{"a":{"b":{"c":1}}}`, `[{"DiffType":"change","First":1,"Second":{"c":1},"Path":["a","b"],"PathDot":"a.b","PathDotStar":"a.b"}]`},
		{`{"a":1}`, `{"a":[]}`, `[{"DiffType":"change","First":1,"Second":[],"Path":["a"],"PathDot":"a","PathDotStar":"a"}]`},
		{`{"a":1}`, `{"a":[1,2]}`, `[{"DiffType":"change","First":1,"Second":[1,2],"Path":["a"],"PathDot":"a","PathDotStar":"a"}]`},
		{`{"a":[1,2,3]}`, `{"a":[1,[1,2],3]}`, `[{"DiffType":"change","First":2,"Second":[1,2],"Path":["a",1],"PathDot":"a[1]","PathDotStar":"a[*]"}]`},
		{`[1,2]`, `[1,{}]`, `[{"DiffType":"change","First":2,"Second":{},"Path":[1],"PathDot":"[1]","PathDotStar":"[*]"}]`},
		{`{"a":[1,2,3]}`, `{"a":1}`, `[{"DiffType":"change","First":[1,2,3],"Second":1,"Path":["a"],"PathDot":"a","PathDotStar":"a"}]`},
		{`{"a":{"b":1}}`, `{"a":1}`, `[{"DiffType":"change","First":{"b":1},"Second":1,"Path":["a"],"PathDot":"a","PathDotStar":"a"}]`},
		// {`{"a":[false,true]}`, `{"a":[true,false]}`, `[{"DiffType":"change","First":false,"Second":true,"Path":["a",0],"PathDot":"a[0]","PathDotStar":"a[*]"},{"DiffType":"change","First":true,"Second":false,"Path":["a",1],"PathDot":"a[1]","PathDotStar":"a[*]"}]`}, // todo flaky test
		// {`{"a":1,"b":1}`, `{"a":2,"b":2}`, `[{"DiffType":"change","First":1,"Second":2,"Path":["a"],"PathDot":"a","PathDotStar":"a"},{"DiffType":"change","First":1,"Second":2,"Path":["b"],"PathDot":"b","PathDotStar":"b"}]`},                                         // todo flaky test
		// {`{"b":1,"a":1}`, `{"b":2,"a":2}`, `[{"DiffType":"change","First":1,"Second":2,"Path":["b"],"PathDot":"b","PathDotStar":"b"},{"DiffType":"change","First":1,"Second":2,"Path":["a"],"PathDot":"a","PathDotStar":"a"}]`},                                         // todo flaky test
	}
	for _, tt := range tests {
		expected := JsonLoads(tt.expected_result)
		result := JsonLoads(CompareJSON(tt.first, tt.second))
		if !reflect.DeepEqual(expected, result) {
			t.Error("wrong TestChange result, case:", tt, "\nexpected:", expected, "\ngot:     ", result)
		}
	}
}

func TestRemove(t *testing.T) {
	tests := []struct {
		first           string
		second          string
		expected_result string
	}{
		{`{"a":1}`, `{}`, `[{"DiffType":"remove","First":1,"Second":null,"Path":["a"],"PathDot":"a","PathDotStar":"a"}]`},
		{`{"a":1.1}`, `{}`, `[{"DiffType":"remove","First":1.1,"Second":null,"Path":["a"],"PathDot":"a","PathDotStar":"a"}]`},
		{`{"a":"a"}`, `{}`, `[{"DiffType":"remove","First":"a","Second":null,"Path":["a"],"PathDot":"a","PathDotStar":"a"}]`},
		{`{"a":false}`, `{}`, `[{"DiffType":"remove","First":false,"Second":null,"Path":["a"],"PathDot":"a","PathDotStar":"a"}]`},
		{`{"a":true}`, `{}`, `[{"DiffType":"remove","First":true,"Second":null,"Path":["a"],"PathDot":"a","PathDotStar":"a"}]`},
		{`{"a":null}`, `{}`, `[{"DiffType":"remove","First":null,"Second":null,"Path":["a"],"PathDot":"a","PathDotStar":"a"}]`},
		{`{"a":{"b":2}}`, `{}`, `[{"DiffType":"remove","First":{"b":2},"Second":null,"Path":["a"],"PathDot":"a","PathDotStar":"a"}]`},
		{`{"a":[1,"2"]}`, `{}`, `[{"DiffType":"remove","First":[1,"2"],"Second":null,"Path":["a"],"PathDot":"a","PathDotStar":"a"}]`},
		{`{"a":1,"b":2}`, `{"a":1}`, `[{"DiffType":"remove","First":2,"Second":null,"Path":["b"],"PathDot":"b","PathDotStar":"b"}]`},
		{`{"a":{"b":2,"c":3}}`, `{"a":{"b":2}}`, `[{"DiffType":"remove","First":3,"Second":null,"Path":["a","c"],"PathDot":"a.c","PathDotStar":"a.c"}]`},
		{`[1,2,3]`, `[1,2]`, `[{"DiffType":"remove","First":3,"Second":null,"Path":[2],"PathDot":"[2]","PathDotStar":"[*]"}]`},
		{`{"a":[1,2,3]}`, `{"a":[1,2]}`, `[{"DiffType":"remove","First":3,"Second":null,"Path":["a",2],"PathDot":"a[2]","PathDotStar":"a[*]"}]`},
		{`{"a":[{"b":2},{"b":[{"c":3}]}]}`, `{"a":[{"b":2},{"b":[{}]}]}`, `[{"DiffType":"remove","First":3,"Second":null,"Path":["a",1,"b",0,"c"],"PathDot":"a[1].b[0].c","PathDotStar":"a[*].b[*].c"}]`},
	}
	for _, tt := range tests {
		expected := JsonLoads(tt.expected_result)
		result := JsonLoads(CompareJSON(tt.first, tt.second))
		if !reflect.DeepEqual(expected, result) {
			t.Error("wrong TestRemove result, case:", tt, "\nexpected:", expected, "\ngot:     ", result)
		}
	}
}

func TestAdd(t *testing.T) {
	tests := []struct {
		first           string
		second          string
		expected_result string
	}{
		{`{}`, `{"a":1}`, `[{"DiffType":"add","First":null,"Second":1,"Path":["a"],"PathDot":"a","PathDotStar":"a"}]`},
		{`{}`, `{"a":1.1}`, `[{"DiffType":"add","First":null,"Second":1.1,"Path":["a"],"PathDot":"a","PathDotStar":"a"}]`},
		{`{}`, `{"a":"a"}`, `[{"DiffType":"add","First":null,"Second":"a","Path":["a"],"PathDot":"a","PathDotStar":"a"}]`},
		{`{}`, `{"a":false}`, `[{"DiffType":"add","First":null,"Second":false,"Path":["a"],"PathDot":"a","PathDotStar":"a"}]`},
		{`{}`, `{"a":true}`, `[{"DiffType":"add","First":null,"Second":true,"Path":["a"],"PathDot":"a","PathDotStar":"a"}]`},
		{`{}`, `{"a":null}`, `[{"DiffType":"add","First":null,"Second":null,"Path":["a"],"PathDot":"a","PathDotStar":"a"}]`},
		{`{}`, `{"a":{"b":2}}`, `[{"DiffType":"add","First":null,"Second":{"b":2},"Path":["a"],"PathDot":"a","PathDotStar":"a"}]`},
		{`{}`, `{"a":[1,"2"]}`, `[{"DiffType":"add","First":null,"Second":[1,"2"],"Path":["a"],"PathDot":"a","PathDotStar":"a"}]`},
		{`{"a":1}`, `{"a":1,"b":2}`, `[{"DiffType":"add","First":null,"Second":2,"Path":["b"],"PathDot":"b","PathDotStar":"b"}]`},
		{`{"a":{"b":2}}`, `{"a":{"b":2,"c":3}}`, `[{"DiffType":"add","First":null,"Second":3,"Path":["a","c"],"PathDot":"a.c","PathDotStar":"a.c"}]`},
		{`[1,2]`, `[1,2,3]`, `[{"DiffType":"add","First":null,"Second":3,"Path":[2],"PathDot":"[2]","PathDotStar":"[*]"}]`},
		{`{"a":[1,2]}`, `{"a":[1,2,3]}`, `[{"DiffType":"add","First":null,"Second":3,"Path":["a",2],"PathDot":"a[2]","PathDotStar":"a[*]"}]`},
		{`{"a":[{"b":2},{"b":[{}]}]}`, `{"a":[{"b":2},{"b":[{"c":3}]}]}`, `[{"DiffType":"add","First":null,"Second":3,"Path":["a",1,"b",0,"c"],"PathDot":"a[1].b[0].c","PathDotStar":"a[*].b[*].c"}]`},
	}
	for _, tt := range tests {
		expected := JsonLoads(tt.expected_result)
		result := JsonLoads(CompareJSON(tt.first, tt.second))
		if !reflect.DeepEqual(expected, result) {
			t.Error("wrong TestAdd result, case:", tt, "\nexpected:", expected, "\ngot:     ", result)
		}
	}
}
